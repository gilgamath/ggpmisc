# R package 'ggpmisc' #

[![](http://www.r-pkg.org/badges/version/ggpmisc)](https://cran.r-project.org/package=ggpmisc) [![](http://cranlogs.r-pkg.org/badges/ggpmisc)](http://cran.rstudio.com/web/packages/ggpmisc/index.html) [![](http://cranlogs.r-pkg.org/badges/grand-total/ggpmisc)](http://cran.rstudio.com/web/packages/ggpmisc/index.html)

Package '__ggpmisc__' (Miscellaneous Extensions to 'ggplot2') is a set of extensions to R package 'ggplot2' (>= 2.1.0) which
I hope will be useful when plotting diverse types of data. Currently available stats
add the following statistics, geoms and function:

* `stat_peaks()` find and label local or global maxima in _y_.
* `stat_valleys()` find and label local or global minima in _y_.
* `stat_poly_eq()` add a label for a fitted linear model to a plot, label can be the fitted polynomial equation, R^2, BIC, AIC.
* `stat_fit_deviations()` display residuals from a model fit as segments linking observed and fitted values.
* `stat_fit_residuals()` residuals from a model fit.
* `stat_fit_augment()`	data augmented with fitted values and statistics using package 'broom'.
* `stat_fit_glance()` one-row summary data frame for a fitted linear model using package 'broom'.
* `stat_debug_group()`, `stat_debug_panel()` print data received as input by a stat's group and panel functions respectively. Useful for debugging new statistics and for exploring how ggplot works.
* `geom_debug()` print data received as input by a geom.
* `try_data_frame()` convert an R object into a data frame. Specially useful for plotting time series (using internally package '') which are returned with _x_ data as POSIXct, allowing direct plotting with 'ggplot2' and packages extending 'ggplot2'.

The package [manual](https://cran.r-project.org/web/packages/ggpmisc/ggpmisc.pdf) describes in more detail the items listed above, and the [vignette](https://cran.r-project.org/web/packages/ggpmisc/vignettes/examples.html) gives several examples of plots produced with the package.

Please, see the web site [r4photobiology](http://www.r4photobiology.info) for 
details and update notices. Other packages, aimed at easing photobiology-related
calculations including the quantification of biologically effective radiation
in meteorology are available as part of a suite described at the same
website.

The current release of '__ggpmisc__' is available through [CRAN](https://cran.r-project.org/web/packages/ggpmisc/index.html) 
for R (>= 3.2.0).